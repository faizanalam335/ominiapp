import { MpaaRating } from './mpaa-rating'

export class Default
{
  public id:number
  public name:string
  public imgPath:string
  public userRating:number
  public language: string
  public duration: number
  public description:string
  public label:string
  public genre:string[]
  public mpaaRating=new MpaaRating ()
}
