import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MoviesComponent } from './movies/movies.component';
import { FormsModule} from '@angular/forms'
import { ReactiveFormsModule} from '@angular/forms';
import { MoviedetailsComponent } from './moviedetails/moviedetails.component'
import { MovieService } from './movieservice/movie.service';
import { MoviehubComponent } from './moviehub/moviehub.component';
import { MovieticketsComponent } from './movietickets/movietickets.component';

@NgModule({
   declarations: [
      AppComponent,
      MoviesComponent,
      MoviedetailsComponent,
      MoviehubComponent,
      MovieticketsComponent
   ],
   imports: [
      BrowserModule,
      AppRoutingModule,
      HttpClientModule,
      FormsModule,
      ReactiveFormsModule
   ],
   providers: [MovieService],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
