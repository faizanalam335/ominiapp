import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http'
import { Default } from '../models/default';
import { map} from 'rxjs/operators'
@Injectable({
  providedIn: 'root'
})
export class MovieService {

  movies:Default[]
  constructor(private _http:HttpClient) { }

  getallmovies(){

    return this._http.get<Default[]>("assets/movies.json")

  }
}
