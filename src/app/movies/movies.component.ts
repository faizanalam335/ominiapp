import { Component, OnInit } from '@angular/core';
import { HttpClient} from '@angular/common/http'
import { Default } from '../models/default';
import * as data from '../../movies.json'
import { Router } from '@angular/router';
import { MovieService } from '../movieservice/movie.service';


@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {

  movies:Default[]
  moviefilter:Default[]


  constructor(private _movies:MovieService,
              private _route:Router){

  }

  ngOnInit(){
    this._movies.getallmovies()
                .subscribe((data)=>{
                    this.movies=data
                    this.moviefilter=data
                    this._movies.movies=data
                    console.log("movies data",data)
                })
  }

  DetailView(movieobj:Default){

          this._route.navigate(['details',movieobj.id])
  }

  searchmovies(event){
       let  searchterm=event.target.value
            this.filtermovies(searchterm)
  }


  filtermovies(search:string){
           this.movies=this.moviefilter
                           .filter(x=>x.name.toLowerCase().includes(search.toLowerCase()))
  }


}
