import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MoviesComponent } from './movies/movies.component';
import { MoviedetailsComponent } from './moviedetails/moviedetails.component';


const routes: Routes =[

          {
            path:'movies',component:MoviesComponent,
          },
          {
            path:'details/:id',component:MoviedetailsComponent
          },

          {path:'',redirectTo:'/movies',pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
