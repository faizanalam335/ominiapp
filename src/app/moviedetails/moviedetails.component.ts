import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MovieService } from '../movieservice/movie.service';
import { Default } from '../models/default';

@Component({
  selector: 'app-moviedetails',
  templateUrl: './moviedetails.component.html',
  styleUrls: ['./moviedetails.component.css']
})
export class MoviedetailsComponent implements OnInit {

   parameter:number
   movie:Default
   constructor(private _route:ActivatedRoute,
               private _movies:MovieService){

        this.parameter=this._route
                           .snapshot.params['id']

   }

  ngOnInit(){
            let object=this._movies.movies.filter(x=>x.id==this.parameter)
                       this.movie=object[0]
                       console.log("Movie Object",this.movie)
  }

  editpath(editpath:string){

    let path="../"+editpath

        console.log("path edited",path)
        return path

  }
}
